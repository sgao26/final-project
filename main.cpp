#include "SkipBoGame.h"

#include <fstream>
#include <istream> 
#include <string>
#include <algorithm>
#include <iostream>
#include <cctype>

using std::transform;
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::cin;
using std::ofstream;

int main(int argc, char* argv[]) {
	// Preliminary check to see if # of user-arguments is correct.
	if ((argc != 5) && (argc != 3)) {
	  
		cout << "invalid program usage: invalid number of arguments" << endl;
    	return 1;
	}
	
	// Check if the second argument is true/false and store that as a boolean.
    string flag_str = argv[1];
    bool flag_bool;
    transform(flag_str.begin(), flag_str.end(), flag_str.begin(), ::tolower);
	
    if (flag_str.compare("true") == 0) {
        flag_bool = true;
    } else if (flag_str.compare("false") == 0) {
        flag_bool = false;
    } else {
        cout << "invalid program usage: invalid first argument" << endl;
        return 1;
    }
  
	// Checks the player number and stock size number.
	if (argc == 5) {
		int playNum = atoi(argv[2]);
		int stockNum = atoi(argv[3]);

		if (playNum < 2 || playNum > 6) {
			cout << "invalid program usage: num players must be 2-6" << endl;
			return 1;
		}
		cout << "num players is " << playNum << endl;
		if (playNum * stockNum > 162 || stockNum <= 0) {
			cout << "invalid program usage: bad stock size" << endl;
			return 1;
		}
		cout << "stock size is " << stockNum << endl;
	}

   	SkipBoGame game;
	
	// User input matches style of a new game.
	if (argc == 5) {
		// Checks if the deckfile can be opened.
		ifstream is(argv[4]);
		if (!is.is_open()) {
			cout << "invalid program usage: can't open deck file" << endl;
			return 1;
		}
		is.close();

		// Calls constructor for SkipBoGame that matches new game.
    	SkipBoGame* temp = new SkipBoGame(flag_bool, atoi(argv[2]), atoi(argv[3]), argv[4]);
		game = *temp;

	} else if (argc == 3) {

		// Checks if the save file can be opened.
		ifstream is(argv[2]);
		if (!is.is_open()) {
			cout << "invalid program usage: can't open input game file" << endl;
			return 1;
		}

		SkipBoGame* temp = new SkipBoGame(flag_bool, argv[2]);
		game = *temp;	
	}

	// Boolean and while-loop to control playing the game.
	bool play = true;
	char input;
	string filename;
	bool gameplay = true;

	while (play) {
		game.drawHand();
		cout << endl;
		cout << " >> " + game.getCurPlayer() + " turn next" << endl;
		cout << "(p)lay, (s)ave, or (q)uit ? ";
		
		cin >> input;
		input = std::tolower(input);
		cout << endl;

		switch (input) {
		case 'p':
		 	game.display();
		  	while (gameplay) {
				cout << "(m)ove [start] [end] or (d)raw ? ";
				cin >> input;
				input = std::tolower(input);
				string num1;
				string num2;

				if (input == 'd') {
					
			  		if (!game.checkHand()) {
						cout << "illegal command, try again" << endl;
					} else {
						game.drawHand();
					}
				} else if (input == 'm') {
			  		cin >> num1;
			  		cin >> num2;

			 			if (num1.length() != 1 || num2.length() != 1) {
			 				cout << "illegal command, try again" << endl;
						}

      				if ((num1 >= "0") && (num1 <= "9")) {
						if ((num2 >= "1" && num2 <= "4")) {
							if (game.moveCard(num1[0], num2[0])) {
								if (game.checkWinner()) {
									cout << endl;
									cout << "GAME OVER - " << game.getCurPlayer() << " wins!" << endl;
									return 1;
								}
								gameplay = false;
							} else {
								cout << "illegal command, try again" << endl;
							}

						} else if (num2 >= "a" && num2 <= "d") {
							if (!game.moveCard(num1[0], num2[0])) {
								cout << "illegal command, try again" << endl;
							} else {
								if (game.checkWinner()) {
									cout << endl;
									cout << "GAME OVER - " << game.getCurPlayer() << " wins!" << endl;
									return 1;
								}
							}
			    		} else {
			      			cout << "illegal command, try again" << endl;
			    		}
			  		} else {
			  
			  			cout << "illegal command, try again" << endl;
 					}
			 
				} else {
			  		cout << "illegal command, try again" << endl;
				}
				cout << endl;
				game.display();
		  }
		  game.movePlayer();
		  gameplay = true;
		  break;
		case 's':
			// Open file to write and save Game.
			cout << "save filename: ";
			cin >> filename;
			if (game.saveGame(filename)) {
				return 0;
			} else {
				return 1;
			}
		case 'q':
			play = false;
			cout << "thanks for playing" << endl;
			break;
		default:
			cout << "illegal command, try again" << endl;
		}
	}

	
	// Return 0 upon successful run.
	return 0;
}
