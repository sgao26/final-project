#ifndef _FACEUPPILE_H
#define _FACEUPPILE_H

#include "Pile.h"
#include <iostream>
#include <vector>

class FaceUpPile : public Pile {

public:
	
	FaceUpPile() {}
  	void display() const;
  
};


class PlayPile : public FaceUpPile {
 public:
  PlayPile() {}
};

class DiscardPile : public PlayPile {

public:
  	
	DiscardPile() {}
	void readIn(std::istream& is);

};

class StockPile : public PlayPile {

private:

  int size_stock;

public:

	StockPile() {}
  	StockPile(int size) : size_stock(size) {}
  	bool isEmpty(StockPile stock);
  	void readIn(std::istream& is);

};

class BuildPile : public FaceUpPile {
 public:

        BuildPile() {}
        void readIn(std::istream& is);
        void clear();
        std::vector<Card>::iterator end();
        std::vector<Card>::iterator begin();
        const Card pullCard();
};

#endif
