# Aashay Patel - apate118
# Anish Kosanam - akosana1
# Sammy Gao - sgao26
# Section 3
# Makefile for SkipBo

C++C=g++
C++FLAGS=-std=c++11 -pedantic -Wall -Wextra -g

# Links together object files to create executable - skipbo
skipbo: main.o SkipBoGame.o DrawPile.o FaceUpPile.o Hand.o Player.o Pile.o Card.o
	$(C++C) -o skipbo main.o SkipBoGame.o DrawPile.o FaceUpPile.o Hand.o Player.o Pile.o Card.o

# Compiles main.cpp to create main.o
main.o: main.cpp SkipBoGame.h
	$(C++C) $(C++FLAGS) -c main.cpp

# Compiles SkipBoGame.cpp to create SkipBoGame.o
SkipBoGame.o: SkipBoGame.cpp SkipBoGame.h
	$(C++C) $(C++FLAGS) -c SkipBoGame.cpp

# Compiles DrawPile.cpp to create DrawPile.o
DrawPile.o: DrawPile.cpp DrawPile.h Pile.h
	$(C++C) $(C++FLAGS) -c DrawPile.cpp

# Compiles FaceUpPile.cpp to create FaceUpPile.o
FaceUpPile.o: FaceUpPile.cpp FaceUpPile.h Pile.h
	$(C++C) $(C++FLAGS) -c FaceUpPile.cpp

# Compiles Hand.cpp to create Hand.o
Hand.o: Hand.cpp Pile.h Hand.h
	$(C++C) $(C++FLAGS) -c Hand.cpp

# Compiles Player.cpp to create Player.o
Player.o: Player.cpp Pile.h Player.h
	$(C++C) $(C++FLAGS) -c Player.cpp

# Compiles Pile.cpp to create Pile.o
Pile.o: Pile.cpp Pile.h Card.h
	$(C++C) $(C++FLAGS) -c Pile.cpp
    
# Compiles Card.cpp to create Card.o
Card.o: Card.cpp Card.h
	$(C++C) $(C++FLAGS) -c Card.cpp

# Removes all object files and the executable named skipbo
clean:
	rm -f *.o skipbo
