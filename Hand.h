#ifndef _HAND_H
#define _HAND_H

#include "Pile.h"

class Hand : public Pile {
	
	public:
	
	Hand() {}
	void readIn(std::istream& is);
  	void display() const;
  	virtual void addCard(const Card& c);
  	const Card removeCard(int idx);
	void insertCard(int num, const Card& c) { pile.insert(pile.begin()+num, c); }
};

#endif
