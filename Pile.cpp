#include <vector>
#include "Pile.h"
#include <iterator>

using std::vector;

// for saving game state - must not change!
std::string Pile::toString() const {
  std::string result;
  result.append(std::to_string(size()));
  result.append("\n");
  // add all the pile elements to the string, at most 20 per line
  for (int i=0; i < size(); ++i) {
    if (i % 20 == 0 && i != 0)
      result.append("\n");
    else if (i != 0)
      result.append(" ");
    result.append(pile[i].toString());
  }
  result.append("\n");
  return result;
}
  
void Pile::readIn(std::istream & is) {
  int cur_value;
  while(is >> cur_value){
    Card cur_card(cur_value);
	// TODO: Change push_back to addCard
    pile.push_back(cur_card);
  }
}

const Card Pile::pullCard() {
	Card temp = pile.back();
	pile.pop_back();
	return temp;
}

/** Clears the pile
 */
void Pile::clear(){
  pile.clear();
}

vector<Card>::iterator Pile::begin(){
  return pile.begin();
}

vector<Card>::iterator Pile::end(){
  return pile.end();
}
