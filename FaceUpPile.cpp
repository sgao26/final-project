#include <iostream>
#include "FaceUpPile.h"
#include <vector>
#include <iterator>

using std::istream;
using std::vector;
using std::iterator;

// for live game play - must not change!
void FaceUpPile::display() const {
	if (size()) {
		pile[pile.size()-1].display();
  	} else std::cout << "--";
  	std::cout << "/" << size();
}

const Card BuildPile::pullCard(){
  Card temp(3);
  return temp;
}

void BuildPile::readIn(istream& is) {
	// The number of cards to put in the Build Pile.
	int build_cards;
	is >> build_cards;

	if (build_cards > 0) {
		int card_num;

		for (int i = 0; i < build_cards; i++) {
			is >> card_num;
			Card temp(card_num);
			addCard(temp);
		}
	}
}

/** To clear build pile after reaches capacity
 */
void BuildPile::clear(){
  pile.clear();
}

vector<Card>::iterator BuildPile::begin(){
  return pile.begin();
}

vector<Card>::iterator BuildPile::end(){
  return pile.end();
}

void DiscardPile::readIn(istream& is) {
	// The number of cards to put in the discard Pile.
	int discard_cards;
	is >> discard_cards;

	if (discard_cards > 0) {
		int card_num;

		for (int i = 0; i < discard_cards; i++) {
			is >> card_num;
			Card temp(card_num);
			addCard(temp);
		}
	}
}

bool StockPile::isEmpty(StockPile stock){
	return stock.pile.empty();
}

void StockPile::readIn(istream& is) {
	int card_num;
	for (int i = 0; i < size_stock; i++) {
		is >> card_num;
		Card temp(card_num);
		addCard(temp);
	}
}

