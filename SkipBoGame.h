// Aashay Patel - apate118
// Anish Kosanam - akosana1
// Sammy Gao - sgao26
// Section 3 - 12/07/18
// SkipBoGame.h

#ifndef _SKIPBOGAME_H
#define _SKIPBOGAME_H

#include <vector>
#include <sstream>

#include "Pile.h"
#include "Player.h"
#include "DrawPile.h"

class SkipBoGame {

private:

  	// Number of players.
  	int nump;
 	// Current player index.
 	int curp;
	int stock_size;
  	bool shuffle;
	// Vector of players.
  	std::vector<Player> peep;
  	BuildPile build[4];
  	DrawPile draw;
	std::vector<Card> replenish_vec;
public:

	bool checkHand() { return (peep[curp].handSize() == 0); }
	
	void checkDraw();

	void movePlayer();
	
	bool moveCard(char start, char end);

	void drawHand();

	/**Checks if draw is empty and if so replenishes maybe not used
	 */
	void replenish();

	/** Checks if the build pile addition is correct
	 */
	void add_to_build(int num_build, const Card& c);

	/** Displays the current game state on the screen for the user.
	 */
  	void display() const;

	/** Writes the game to string, used for saving the game.
	 * returns string containing the relevant game information to load again
	 */
	std::string toString() const;

	/** Constructor for creating a temporary Game object.
	 */
	SkipBoGame() {}

  	/** Constructor for loading a Saved Game.
	 */
  	SkipBoGame(bool shuffle_flag, std::string load_file);
  	
	/** Constructor for creating a New Game.
	 */
  	SkipBoGame(bool shuffle_flag, int players, int stockpile, std::string deck_file);
  
	/** Reads into a txt file that should be correctly formed in order to load
	 * a saved game.
	 */
	void readIn(std::istream& is);

	/** Reads into a deck file that should be correctly formed in order to fill
	 * the draw pile of cards to start the game setup.
	 */
	void fill_draw(std::istream& is);

	/** Deals the cards from the draw pile.
	 */
	void deal();

	/** Generates a random player as the curp
	 */
	void randPlayer();
	
	/** Gets the number of players in the game.
	 */
	int getPlayers() { return nump; }

	/** Gets the stockpile size used in the game.
	 */
	int getStockSize() { return stock_size; }

	/** Gets the current player's name.
	 */
	std::string getCurPlayer();

	bool saveGame(std::string filename);
	
	bool checkWinner();
};

#endif // _SKIPBOGAME_H
