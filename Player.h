// Aashay Patel - apate118
// Anish Kosanam - akosana1
// Sammy Gao - sgao26
// Section 3 - 12/5/18
// Player.h

#ifndef _PLAYER_H
#define _PLAYER_H

#include "FaceUpPile.h"
#include "Hand.h"

#include <vector>
#include <string>

class Player {

	private:

  	DiscardPile discard[4];
  	StockPile stock;
  	Hand hand;
  	std::string name;

 	public:

	Player (std::string player_name, int stock_size);
	int checkStock() { return stock.size(); }
	void display() const;
 	std::string toString() const;
 	void readIn(std::istream& is);
	int handSize() { return hand.size(); }
	void addStock(const Card& c) { stock.addCard(c); }
	void addHand(const Card& c) { hand.addCard(c); }
	std::string getName() { return name; }
	Card removeHand(int index) { return hand.removeCard(index); }
	int discardSize(int index) { return discard[index].size(); }
	Card removeDiscard(int index) { return discard[index].pullCard(); }
	Card removeStock() { return stock.pullCard(); }
	void addDiscard(int index, Card card) { discard[index].addCard(card); }
	void insertHand(int num, const Card& c) { hand.insertCard(num, c); } 
};

#endif
