// Aashay Patel - apate118
// Anish Kosanam - akosana1
// Sammy Gao - sgao26
// Section 3 - 12/05/18
// Player.cpp

#include "Player.h"
#include "FaceUpPile.h"

#include <iostream>
#include <string>
#include <sstream>

using std::string;
using std::istream;

Player::Player(string player_name, int stock_size) {
	name = player_name;
	StockPile* temp = new StockPile(stock_size);
	stock = *temp;
}

// for live game play - must not change!
void Player::display() const {
  	std::cout << "Current ";
  	std::cout << name << ":  Stock{0}: " ;
  	stock.display();
  	std::cout << std::endl;
  	std::cout << "   Discards{1-4}: ";
  	
	for (int i = 0; i < 4; ++i) {
    	discard[i].display();
    	std::cout << "  ";
  	}
  	
	std::cout << std::endl;
  	std::cout << "   Hand{5-9}: ";
  	hand.display();
  	std::cout << std::endl;
}

/* saving state format - must not change!
PlayerName
Stock size
01 02 03  ...
...
Hand size
01 02 03 04 05
Discard1 size
01 02 03 04 ...
...
Discard2 size
...
Discard3 size
...
Discard4 size
...
*/
std::string Player::toString() const {
  	std::stringstream result;
  	result << name << "\n";
  	result << "Stock " << stock.toString();
  	result << "Hand " << hand.toString();
  
  	for (int i = 0; i < 4; ++i) {
    	result << "Discard" << i << " " << discard[i].toString();
  	}
  	
	return result.str();
}

void Player::readIn(istream& is) {
	string skip;
  	
	// Reads in cards for the stockpile.
	stock.readIn(is);

  	// Skips "Hand" and reads in cards for the hand.
   	is >> skip;	
	hand.readIn(is);
  
 	for (int i = 0; i < 4; i++) {
    	is >> skip;
    	DiscardPile* temp = new DiscardPile();
    	temp->readIn(is);
    	discard[i] = *temp;
  	}
} 


