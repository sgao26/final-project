// Aashay Patel - apate118
// Anish Kosanam - akosana1
// Sammy Gao - sgao26
// Section 3 - 12/5/18
// SkipBoGame.cpp

#include "SkipBoGame.h"

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <ctime>

using std::vector;
using std::cout;
using std::endl;
using std::transform;
using std::string;
using std::stringstream;
using std::istream;
using std::ifstream;
using std::ofstream;

bool SkipBoGame::checkWinner() {
	return (peep[curp].checkStock() == 0);
}

void SkipBoGame::movePlayer() {
	curp = (curp + 1) % nump;
}

void SkipBoGame::checkDraw() {
	if (draw.size() == 0) {
		draw.replenish_draw(replenish_vec);
		if (shuffle) {
			draw.shuffleDeck();
		}
	}
}

bool SkipBoGame::moveCard(char start, char end) {
	int	num = start - '0';
	int dest;
	Card temp;
	
	if (num >= 5 && num <= 9) {
		if (peep[curp].handSize() == 0 || peep[curp].handSize() <= num - 5) {
			return false;
		}
		temp = peep[curp].removeHand(num - 5);
	} else if (num >= 1 && num <= 4) {
		num -= 1;
		if (peep[curp].discardSize(num) == 0) {
			return false;
		}
		temp = peep[curp].removeDiscard(num);
	} else {
		temp = peep[curp].removeStock();
	}
	
	if (end >= 'a' && end <= 'd') {
		dest = end - 'a';
		if (build[dest].size() == 0 && (temp.getValue() == 1 || temp.getValue() == 0)) {
			add_to_build(dest, temp);
			return true;
		
		} else if (build[dest].size() == temp.getValue() + 1 || (temp.getValue() == 0)) {
			add_to_build(dest, temp);
			return true;
		} else {
			if (num >= 5 && num <= 9) {
				peep[curp].insertHand(num - 5, temp);
			} else if (num >= 1 && num <= 4) {
				peep[curp].addDiscard(num - 1, temp);
			} else {
				peep[curp].addStock(temp);
			}
			return false;
		}
	} else {
		dest = end - '0';
		dest -= 1;
		peep[curp].addDiscard(dest, temp);
	}
	return true;
}

bool SkipBoGame::saveGame(string filename) {
	bool success = 0;	
	ofstream save;

	save.open(filename);
	if (!save.is_open()) {
		cout << "Error: Unable to successfully save game to the text file" <<
			endl;
		return success;
	}
	draw.replenish_draw(replenish_vec);
	save << toString();
	save.close();
	success = true;
	return success;
}
/** Reads into a deck file that should be correctly formed in order to fill the
 * draw pile of cards to start the game setup.
 */
void SkipBoGame::fill_draw(istream& is) {
	int card_num;
	while (is >> card_num) {
		Card temp(card_num);
		draw.addCard(temp);
	}
}

/** Deals the cards from the draw pile.
 */
void SkipBoGame::deal() {
	// Deals out cards to each player's stockpile.
	for (int cards = 0; cards < stock_size; cards++) {
		for (int i = 0; i < nump; i++) {
			checkDraw();
			const Card temp = draw.pullCard();	
			peep[i].addStock(temp);
		}
	}
}

void SkipBoGame::drawHand() {
	
	// Deals out cards to the current player's hand.	
	for (int cards = peep[curp].handSize(); cards < 5; cards++) {
		checkDraw();
		const Card temp = draw.pullCard();
		peep[curp].addHand(temp);
	}
}

/** Reads into a txt file that should be correctly formed in order to load a 
 * saved game.
 */
void SkipBoGame::readIn(istream& is) {
	string flag_str, name, skip;
	bool flag_bool;
	int stock_size;

	is >> flag_str;
	transform(flag_str.begin(), flag_str.end(), flag_str.begin(), ::tolower);

	// Check for the boolean flags
	if (flag_str.compare("true") == 0) {
		flag_bool = true;
	} else if (flag_str.compare("false") == 0) {
		flag_bool = false;
	}

	is >> nump;
	is >> curp;

	// Players set up.
	for (int i = 0; i < nump; i++) {
        is >> name;
    	// Skip the "Stock" label.
    	is >> skip;
  		is >> stock_size;
		Player* temp = new Player(name, stock_size);
    	temp->readIn(is);
    	peep.push_back(*temp);
	}
	
	// Skip the "Draw" Label.
	is >> skip;
	// Draw pile set up.
	draw.readIn(is);

	if (shuffle && !flag_bool) {
		draw.shuffleDeck();
	}

	// Build piles set up.
	for (int i = 0; i < 4; i++) {
		is >> skip;
		(build[i]).readIn(is);
	}
}

/** Constructor for loading a Saved Game.
 */
SkipBoGame::SkipBoGame(bool shuffle_flag, string load_file) {
	shuffle = shuffle_flag;
	
	for (int i = 0; i < 4; i++) {
		BuildPile* temp = new BuildPile();
		build[i] = *temp;
	}
	
	DrawPile* temp = new DrawPile(shuffle);
	draw = *temp;

	// Open the filestream to read the save game textfile.
	ifstream is(load_file);
	readIn(is);	
}

/** Constructor for creating a New Game.
 */
SkipBoGame::SkipBoGame(bool shuffle_bool, int players, int stockpile, string deck_file) {
	
	shuffle = shuffle_bool;
	curp = 0;
	nump = players;
	stock_size = stockpile;

	// Create the players.
	for (int i = 0; i < nump; i++) {
		// Create a player name according to number.
		stringstream ss;
		ss << "Player" << i;

    	Player* temp = new Player(ss.str(), stockpile);
    	peep.push_back(*temp);
  	}

	// Create the 4 buildpiles.
  	for (int i = 0; i < 4; i++) {
    	BuildPile* temp = new BuildPile();
    	build[i] = *temp;
  	}

	DrawPile* temp = new DrawPile(shuffle);
	draw = *temp;

	// Fill the draw pile using the deck.
	ifstream is(deck_file);
	fill_draw(is);
	
	// Check if the cards need to be shuffled in the Draw Pile.
	if (shuffle) {
		draw.shuffleDeck();
		randPlayer();
	}

	// Deal the cards to the stocks to start the game.
	deal();	
}

/* for live game play - must not change format!

drawPile  build_a  build_b  build_c  build_d
playerName  stock_0  
discards: discard_1 discard_2 discard_3 discard_4
hand: card_5 card_6 card_7 card_8 card_9
 */
void SkipBoGame::display() const {
	
	std::cout << "Draw: ";
	draw.display();
	std::cout << "  Build Piles: ";

  	for (int j = 0; j < 4; j++) {
    	build[j].display();
    	std::cout << " ";
  	}

  	std::cout << std::endl;
  	peep[curp].display();
}

/* for saving state - must not change format!

shuffle numplayers currplayer
PlayerCurp [display]
PlayerCurp+1 [display]
[etc for all players]
Draw [display]
Build_a [display]
Build_b [display]
Build_c [display]
Build_d [display]
*/
std::string SkipBoGame::toString() const {
  	
	std::stringstream result;
  	int idx;
  	result << draw.getRand() << " " << nump << " " << curp << "\n";
  	
	for (int i = 0; i < nump; ++i) {
    	idx = (curp+i) % nump;
    	result << peep[idx].toString();
  	}
  	
	result << "Draw " << draw.toString(); 
  	
	for (int j = 0; j < 4; j++) {
    	result << "Build_" << char('a'+j) << " ";
    	result << build[j].toString();  
  	}

	return result.str();
}

/** Generates a random player as the curp
 */
void SkipBoGame::randPlayer(){
  std::srand(std::time(nullptr));
  int rand_num = std::rand();
  int rand_player = rand_num % nump;
  curp = rand_player;
}

/** Gets the current player's name.
 */
string SkipBoGame::getCurPlayer() {
	return peep[curp].getName();
}

void SkipBoGame::add_to_build(int num_build, const Card& c){
  build[num_build].addCard(c);
  if(build[num_build].size() == 12){
    replenish_vec.insert(replenish_vec.end(), build[num_build].begin(), build[num_build].end());
    build[num_build].clear();
    return;
  }
 
}
