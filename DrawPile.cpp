#include <iostream>
#include <algorithm>
#include <vector>

#include "DrawPile.h"

using std::istream;
using std::string;
using std::vector;

// for live game play - must not change!
void DrawPile::display() const {
  std::cout << "[XX]/" << size();
}

string DrawPile::getRand() const {
	if (rand) {
		return "true";
	} else {
		return "false";
	}
}


void DrawPile::shuffleDeck() {
  random_shuffle(begin(), end());
}

void DrawPile::replenish_draw(vector<Card> rep_pile) {
  for(vector<Card>::iterator iter = rep_pile.begin(); iter != rep_pile.end(); iter++){
    addCard(*iter);
  }


}

void DrawPile::readIn(istream& is) {
	int draw_cards;
	is >> draw_cards;
	
	if (draw_cards > 0) {
		int card_num;

		for (int i = 0; i < draw_cards; i++) {
			is >> card_num;
			Card temp(card_num);
			addCard(temp);
		}
	}
}
