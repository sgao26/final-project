#include <iostream>
#include "Hand.h"

using std::istream;
using std::string;

/*************************************
For SkipBo - Fall 2018 - EN.601.220
Instructor provided code
*************************************/

// for live game play - must not change!
void Hand::display() const {
  int i;
  for (i = 0; i < size(); i++) {
    pile[i].display();
    std::cout << "  ";
  }
  for ( ; i < 5; i++)
    std::cout << "--  ";
}

void Hand::addCard(const Card& c){
  if(size() == 5){
    std::cout << "illegal command, try again" << std::endl;
  } else {
    pile.push_back(c);
  }
}

const Card Hand::removeCard(int idx) {
    const Card temp = pile[idx];
	pile.erase(pile.begin() + idx);
    return temp;
}

void Hand::readIn(istream& is) {
	// The number of cards to hold in the Hand.
	int hand_cards;
	is >> hand_cards;
	if (hand_cards > 0) {
		int card_num;

		for (int i = 0; i < hand_cards; i++) {
			is >> card_num;
			Card temp(card_num);
			addCard(temp);
		}
	}
}

