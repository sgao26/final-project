#ifndef _DRAWPILE_H
#define _DRAWPILE_H

#include "Pile.h"
#include <vector>

class DrawPile : public Pile {

private:

	bool rand;

public:
	
	DrawPile() {}
	DrawPile(bool shuffle) : rand(shuffle) {}
  	void display() const;
	std::string getRand() const;
	void readIn(std::istream& is);
	void shuffleDeck();
	
	void replenish_draw(std::vector<Card> rep_pile);

};

#endif
